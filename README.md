Verhaltenskodex “eJousting”

 
0. Allgemeines
eJousting versteht sich nicht als eingetragener Verein.
In diesem Kodex werden Regel definiert, an die sich alle Mitglieder zu halten haben.
Die Mitglieder sind in einem nicht eingetragenen Verein/Verband organisiert. 
 
1. Mitglieder
 
1.1 Erklärung Level
Mitglieder von “eJousting” befinden sich auf einem von vier Leveln.
Die Rechte und Pflichten eines Levels enthalten immer die Rechte und Pflichten des darunterliegenden Levels.
Beispiel: Level 2 beinhaltet alle Rechte und Pflichten von 1 und 0.
D.h. dürfen alle Level 1 wählen, dürfen alle Level 2 und drüber auch wählen. 
Sollte die Bezeichnung “gilt für Level X und höher” fehlen, gilt diese, außer sie wird speziell verneint.
 
1.1.1 Level 1
	Mitglieder auf diesem Level beteiligen sich bei der Umsetzung von Events.
	Mitglieder auf Level 2 oder höher können Mitglieder auf Level 0 einführen.
 
1.1.2 Level 1
	Mitglieder auf diesem Level unterstützen “eJousting” regelmäßig bei Events.
	Mitglieder welche sich auf Level 0 befinden, können mit einer einfachen Mehrheit von Mitgliedern die sich auf Level 2 und höher befinden auf Level 1 gewählt werden.
 
1.1.3 Level 2
	Mitglieder auf diesem Level sind hauptsächlich für die Gestaltung und Umsetzung aller “eJousting” Events verantwortlich. Außerdem wird von Level 2 Mitgliedern eine regelmäßige Teilnahme am Vereinsleben erwartet.
	Mitglieder welche sich auf Level 1 befinden, können mit einer absoluten Mehrheit von Mitgliedern auf Level 2 und höher auf Level 2 gewählt werden.
 
1.1.4 Top Level
	Mitglieder auf diesem Level können unter sich kleine bis mittelgroße Entscheidungen ohne Rücksprache mit Level 2 und niedriger treffen. Außerdem dürfen Top Level Mitglieder Einkäufe bis 80€ ohne Rücksprache mit Level 2 und niedriger tätigen.
	Es sind max. 3 aber mind. 2 Top Level Mitglieder nötig. Mitglieder die sich auf Level 2 befinden, können mit einer absoluten Mehrheit von level 1 und 2 Mitgliedern auf das Top Level gewählt werden.
 
 
 1.4 Abwahl Posten
Gegen Posten dürfen Mitglieder auf Level 2 und höher ein Misstrauensvotum stellen.
	Posten können mit triftigem Grund ( z.B. Posten Inkompetenz ) von Mitglieder auf Level 2 und höher mit einer absoluten Mehrheit abgewählt werden. 
 
1.5 Abwahl Top Level
	Gegen Mitglieder auf dem Top Level dürfen Mitglieder auf Level 2 und höher ein Misstrauensvotum stellen.
Das Misstrauensvotum bedarf eines triftigen Grundes.
Dieses muss mit einer absoluten Mehrheit von Level 2 und höher bestätigt werden um das Top Level Mitglied zu entfernen.
 
1.6 Level Abstieg
Ein Misstrauensvotum darf nur von Mitgliedern auf derselben Stufe oder höher gestellt werden ( ausgenommen Top Level ).
	Jedes Mitglied kann mit einer absoluten Mehrheit von Level 2 und höher, ein Level herab gestuft werden.
 
1.6.1 Ausschluss vom Verein
Gegen Mitglieder dürfen Mitglieder auf Level 2 und höher ein Misstrauensvotum stellen.
Level 1 und niedriger dürfen bei einem Level 2 und höher einen Antrag auf Misstrauensvotum stellen. ( Dient dem Schutz von kleinen Zenkerein unter den Mitgliedern)
Das Misstrauensvotum bedarf eines triftigen Grundes.
Dieses muss mit einer absoluten Mehrheit von Level 2 und höher bestätigt.
 
1.6.2 Streitschlichtung
	Ein Streit der den Verein angeht, wird im Verein besprochen.
Dazu kann ein Streitschlichter Stufe 2 oder höher angesprochen werden.
 
1.7 Bezeichnung triftiger Grund
Bsp.: Wiederholte Missachtung von Regeln, 
Diebstahl, Unterschlagung, Beschädigung, Stören des Vereinslebens, Absichtliches Fehlverhalten, keine Teilnahme am Vereinsleben über einen Zeitraum > 6 Monate ohne Entschuldigung/Grund

 1.7.1 Nicht triftiger Grund 
keine Teilnahme am Vereinsleben über einen Zeitraum < 6 Monate
keine Teilnahme am Vereinsleben über einen Zeitraum > 6 Monate mit Entschuldigung/Grund
 
 
2. Abstimmungen
	Abstimmungen sollten immer in Form eines Meetings erfolgen, kleine Entscheidungen können jedoch auch über diverse Planungsgruppen online entschieden werden.
 
2.1 Mehrheiten
Einfach Mehrheit ist 50% aller Stimmen
Absolute Mehrheit entspricht 66% aller Stimmen.
Beispiel: 2/2/1 sind 50% damit angenommen.


2.2 Abstimmungen
	Um Entscheidungen treffen zu können müssen mindestens 4 Mitglieder von Level 2 und höher anwesend sein, wobei 1 der 4 Mitglieder auf dem Top Level ist.
 
2.2.1 Stimmberechtigte für Regeländerungen
	Alle Mitglieder auf Level 2 oder höher sind stimmberechtigt. 
 
2.3 Verhalten bei Uneinigkeit
	Sollte sich bei einer Abstimmung keine Entscheidung finden so können Mitglieder auf Level 2 und höher abstimmen ob bei der Entscheidungsfindung Mitglieder auf Level 1 hinzugezogen werden sollen.
 
2.4 Satzungsänderungen
	Ein Antrag auf Satzungsänderung darf von jedem Mitglied auf Level 1 und höher gestellt werden. Dieser Antrag muss bei einem offiziellen Meeting gestellt werden. Zur Abstimmung sind alle Mitglieder auf Level 1 und höher berechtigt. Der Satzungsänderung muss mit einer absoluten Mehrheit zugestimmt werden um übernommen zu werden. 
 
2.5 Versammlungen
2.5.1 Vollversammlung
	Alle Mitglieder 0+
2.5.2 Kleine Versammlung
	Ab Level 1
2.5.3 Kleiner Rat
	Ab Level 2
2.5.4 Stab
	Top Level
 
3. Posten
	Innerhalb von “eJousting” werden 4 Posten verteilt.
 
3.1 Postensupport
	Posten dürfen nur von Mitglieder auf Level 2 und höher begleitet werden. Jedes Mitglied auf Level 1 und höher darf Posten unterstützen.
 
 
3.2 Posten
 
3.2.1 Kasse
	Dieser Posten verwaltet das Geld innerhalb von “eJousting” und ist dafür verantwortlich zu Events die Kasse herauszugeben und nach dem Event die Kasse wieder in Empfang zu nehmen. Außerdem zählt der Posten das Geld vor und nach jedem Event.
 
3.2.2 Öffentlichkeitsarbeit
	Dieser Posten betreibt Werbung auf verschiedenen sozialen Medien und ist für den Kontakt zu Sponsoren verantwortlich. 
 
3.2.3 Abendgestaltung
	Dieser Posten ist für den Ablauf aller Events verantwortlich, dazu gehört, das für alle anfallenden Aufgaben während eines Events Mitglieder zur Verfügung stehen. Des weiteren ist dieser Posten für die Technik während eines Events verantwortlich d.h. das allen Stationen genügend Controller/Monitore etc. zur Verfügung stehen.
 
3.2.4 Beschaffung
	Dieser Posten sollte sich einen Überblick über die vorhandene Technik machen und bei Bedarf neue Technik beschaffen bzw. alte Technik ersetzen. Des weiteren ist dieser Posten für die Beschaffung von Preisen zuständig. 
 
3.3 Neue Posten 
	Können durch Abstimmung kleinen Rat, gebildet werden 
 
4. Weiteres
	Wir achten und schätzen uns und andere. Wir achten das Eigentum des Vereins, sowie Anderer.
 
5.  Salvatorische Klausel
 
Sollten einzelne Bestimmungen dieses Vertrages unwirksam oder undurchführbar sein oder nach Vertragsschluss unwirksam oder undurchführbar werden, bleibt davon die Wirksamkeit des Vertrages im Übrigen unberührt. An die Stelle der unwirksamen oder undurchführbaren Bestimmung soll diejenige wirksame und durchführbare Regelung treten, deren Wirkungen der wirtschaftlichen Zielsetzung am nächsten kommen, die die Vertragsparteien mit der unwirksamen bzw. undurchführbaren Bestimmung verfolgt haben. Die vorstehenden Bestimmungen gelten entsprechend für den Fall, dass sich der Vertrag als lückenhaft erweist.
